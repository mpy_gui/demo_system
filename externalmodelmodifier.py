"""
Simulation of an external source which modifies the model data
"""

import threading
import random
import time

class EModify:
    def __init__(self, model):
        self.model = model

    def modifyModel(self):
        while (True):
            flag = random.randint(0,1)
            if(flag == 1):
                ch = random.randint(32,127)
                print("External source modifies model with char: "+chr(ch))
                self.model.putc(chr(ch))
            time.sleep(2)

    def startExtModModifier(self):
        self.exthr = threading.Thread(target=self.modifyModel)
        self.exthr.start()