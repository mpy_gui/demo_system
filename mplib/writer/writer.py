"""
The writer module is used to render text on the screen. To achieve this, it needs the font definitions which are
saved as Python dictionaries. The fonts can be created with the font creator tool.
"""

import mpywrapper as mp
import sys


def import_module(name):
    '''
    import_module is needed to make the import of modules at runtime possible and to provide error handling
    the function is called by writer.setfont
    :param name: module name (without .py)
    '''
    try:
        # name = name.strip().split()
        statement = "import {}".format(name)
        exec(statement, globals())
        return sys.modules[name]
    except:
        print("error importing module")
        return


class Writer(object):
    """
    class Writer
    """
    def __init__(self):
        # reference to the current module
        # WARNING : this is the `module` object itself
        # not it's name
        self.current_module = None
        # {name:module} mapping
        self.imported_fonts = {}

    def setfont(self, fontname, fontsize):
        self.current_module = self._loadfont(fontname, fontsize)

    def _loadfont(self, fontname, fontsize):
        name = fontname + str(fontsize)
        if name not in self.imported_fonts:
            self.imported_fonts[name] = import_module(name)
        return self.imported_fonts[name]

    def putc_ascii(self, ch, xpos, ypos):
        width, height, bitmap = self._get_char(ch)
        self._drawc(height, width, bitmap, xpos, ypos)
        return xpos + width

    def putc(self, ch, xpos, ypos):
        width, height, bitmap = self._get_char(ord(ch))
        self._drawc(height, width, bitmap, xpos, ypos)
        return xpos + width

    def putstr(self, string, xpos, ypos):
        strlen = len(string)
        xoffset = 0
        for ch in range(strlen):
            width, height, bitmap = self._get_char(ord(string[ch]))
            self._drawc(height, width, bitmap, xpos + xoffset, ypos)
            xoffset += width + 1
        return xoffset - 1

    def _drawc(self, height, width, bitmap, xpos, ypos):
        for y in range(height):
            for x in range(width):
                if bitmap[y * width + x] == 1:
                    mp.gfx.draw_pixel(xpos + x, ypos + y)

    def _get_char(self, chrnum):
        try:
            return self.current_module.characters[chrnum]
        except KeyError:
            return self.current_module.characters[63]

# create singleton
writerObj = Writer()


# module functions
def setfont(fontname, fontsize):
    """
    set the font of the writer. the font will be used until the next call of setfondt
    if the module of the font is not loaded, it be tried to load it
    Usage e.g.:
        self.setfont("arial", "14")
    :param fontname: Fontname (without .py)
    :param fontsize: Fontsize
    """
    writerObj.setfont(fontname, fontsize)


def putc(ch, xpos, ypos):
    """
    render a character to the screen
    :param ch: character to draw
    :param xpos: left x coordinate of the character
    :param ypos: upper y coordinate of the character
    :return: right x position of the drawn character
    """
    return writerObj.putc(ch, xpos, ypos)

def putc_ascii(ch, xpos, ypos):
    """
    render a character with the passed ascii code to the screen, if the ascii code is not in the font dictionary
    then a question mark will be drawn
    :param ch: ascii code of the character to draw
    :param xpos: left x coordinate of the character
    :param ypos: upper y coordinate of the character
    :return: right x position of the drawn character
    """
    return writerObj.putc_ascii(ch, xpos, ypos)


def putstr(string, xpos, ypos):
    """
    render a string to the screen
    :param string: the string to draw
    :param xpos: left x coordinate of the string
    :param ypos: upper y coordinate of the string
    :return: right x position of the drawn string
    """
    return writerObj.putstr(string, xpos, ypos)
