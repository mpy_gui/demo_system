"""
The uasyncio module is needed to acheive concurrent execution of Python code. This module is only used on the embedded
system since there exists a CPython implementation as well. This module is an adaption of the uasyncio module which is
already contained in micropython-lib on github:
https://github.com/micropython/micropython-lib/blob/master/uasyncio/uasyncio/__init__.py
"""

import uerrno
import uselect as select
import utime as time
import utimeq

type_gen = type((lambda: (yield))())

class CancelledError(Exception):
	pass

class TimeoutError(CancelledError):
	pass

class EventLoop:
	def __init__(self, len=42):
		self.q = utimeq.utimeq(len)
		# Current task being run. Task is a top-level coroutine scheduled
		# in the event loop (sub-coroutines executed transparently by
		# yield from/await, event loop "doesn't see" them).
		self.cur_task = None

	def time(self):
		return time.ticks_ms()

	def create_task(self, coro):
		# CPython 3.4.2
		self.call_later_ms(0, coro)
		# CPython asyncio incompatibility: we don't return Task object

	def call_soon(self, callback, *args):
		self.call_at_(self.time(), callback, args)

	def call_later(self, delay, callback, *args):
		self.call_at_(time.ticks_add(self.time(), int(delay * 1000)), callback, args)

	def call_later_ms(self, delay, callback, *args):
		self.call_at_(time.ticks_add(self.time(), delay), callback, args)

	def call_at_(self, time, callback, args):
		self.q.push(time, callback, args)

	def wait(self, delay):
		# Default wait implementation, to be overriden in subclasses
		# with IO scheduling
		time.sleep_ms(delay)

	def run_forever(self):
		#print("running forever")
		cur_task = [0, 0, 0]
		while True:
			if self.q:
				# wait() may finish prematurely due to I/O completion,
				# and schedule new, earlier than before tasks to run.
				while 1:
					t = self.q.peektime()
					tnow = self.time()
					delay = time.ticks_diff(t, tnow)
					if delay < 0:
						delay = 0
					# Always call wait(), to give a chance to I/O scheduling
					self.wait(delay)
					if delay == 0:
						break
				self.q.pop(cur_task)
				t = cur_task[0]
				cb = cur_task[1]
				args = cur_task[2]
				self.cur_task = cb
			else:
				self.wait(-1)
				# Assuming IO completion scheduled some tasks
				continue
			if callable(cb):
				cb(*args)
			else:
				delay = 0
				try:
					if args == ():
						ret = next(cb)
					else:
						ret = cb.send(*args)
					if isinstance(ret, SysCall1):
						arg = ret.arg
						if isinstance(ret, SleepMs):
							delay = arg
						elif isinstance(ret, IORead):
							cb.pend_throw(False)
							self.add_reader(arg, cb)
							continue
						elif isinstance(ret, IOWrite):
							cb.pend_throw(False)
							self.add_writer(arg, cb)
							continue
						elif isinstance(ret, IOReadDone):
							self.remove_reader(arg)
						elif isinstance(ret, IOWriteDone):
							self.remove_writer(arg)
						elif isinstance(ret, StopLoop):
							return arg
						else:
							assert False, "Unknown syscall yielded: %r (of type %r)" % (ret, type(ret))
					elif isinstance(ret, type_gen):
							self.call_soon(ret)
					elif isinstance(ret, int):
							# Delay
							delay = ret
					elif ret is None:
							# Just reschedule
							pass
					elif ret is False:
						# Don't reschedule
						continue
					else:
						assert False, "Unsupported coroutine yield value: %r (of type %r)" % (ret, type(ret))
				except StopIteration as e:
					continue
				except CancelledError as e:
					continue
				# Currently all syscalls don't return anything, so we don't
				# need to feed anything to the next invocation of coroutine.
				# If that changes, need to pass that value below.
				self.call_later_ms(delay, cb)

	def run_until_complete(self, coro):
		def _run_and_stop():
			yield from coro
			yield StopLoop(0)
		self.call_soon(_run_and_stop())
		self.run_forever()

	def stop(self):
		self.call_soon((lambda: (yield StopLoop(0)))())

	def close(self):
		pass

class SysCall:

	def __init__(self, *args):
		self.args = args

	def handle(self):
		raise NotImplementedError

# Optimized syscall with 1 arg
class SysCall1(SysCall):

	def __init__(self, arg):
		self.arg = arg

class StopLoop(SysCall1):
	pass

class IORead(SysCall1):
	pass

class IOWrite(SysCall1):
	pass

class IOReadDone(SysCall1):
	pass

class IOWriteDone(SysCall1):
	pass

_event_loop = None
# _event_loop_class = EventLoop


def get_event_loop(len=42):
	global _event_loop
	if _event_loop is None:
		_event_loop = EventLoop(len)
	return _event_loop

def sleep(secs):
	yield int(secs * 1000)

# Implementation of sleep_ms awaitable with zero heap memory usage
class SleepMs(SysCall1):

	def __init__(self):
		self.v = None
		self.arg = None

	def __call__(self, arg):
		self.v = arg
		#print("__call__")
		return self

	def __iter__(self):
		#print("__iter__")
		return self

	def __next__(self):
		if self.v is not None:
			#print("__next__ syscall enter")
			self.arg = self.v
			self.v = None
			return self
		#print("__next__ syscall exit")
		_stop_iter.__traceback__ = None
		raise _stop_iter

_stop_iter = StopIteration()
sleep_ms = SleepMs()

def cancel(coro):
	prev = coro.pend_throw(CancelledError())
	if prev is False:
		_event_loop.call_soon(coro)

class TimeoutObj:
	def __init__(self, coro):
		self.coro = coro

def wait_for_ms(coro, timeout):

	def waiter(coro, timeout_obj):
		res = yield from coro
		timeout_obj.coro = None
		return res

	def timeout_func(timeout_obj):
		if timeout_obj.coro:
			prev = timeout_obj.coro.pend_throw(TimeoutError())
			#print("prev pend", prev)
			if prev is False:
				_event_loop.call_soon(timeout_obj.coro)

	timeout_obj = TimeoutObj(_event_loop.cur_task)
	_event_loop.call_later_ms(timeout, timeout_func, timeout_obj)
	return (yield from waiter(coro, timeout_obj))

def wait_for(coro, timeout):
	return wait_for_ms(coro, int(timeout * 1000))

def coroutine(f):
	return f

#
# The functions below are deprecated in uasyncio, and provided only
# for compatibility with CPython asyncio
#

def ensure_future(coro, loop=_event_loop):
	_event_loop.call_soon(coro)
	# CPython asyncio incompatibility: we don't return Task object
	return coro

# CPython asyncio incompatibility: Task is a function, not a class (for efficiency)
def Task(coro, loop=_event_loop):
	# Same as async()
	_event_loop.call_soon(coro)

class PollEventLoop(EventLoop):

	def __init__(self, len=42):
		EventLoop.__init__(self, len)
		self.poller = select.poll()
		self.objmap = {}

	def add_reader(self, sock, cb, *args):
		if args:
			self.poller.register(sock, select.POLLIN)
			self.objmap[id(sock)] = (cb, args)
		else:
			self.poller.register(sock, select.POLLIN)
			self.objmap[id(sock)] = cb

	def remove_reader(self, sock):
		self.poller.unregister(sock)
		del self.objmap[id(sock)]

	def add_writer(self, sock, cb, *args):
		if args:
			self.poller.register(sock, select.POLLOUT)
			self.objmap[id(sock)] = (cb, args)
		else:
			self.poller.register(sock, select.POLLOUT)
			self.objmap[id(sock)] = cb

	def remove_writer(self, sock):
		try:
			self.poller.unregister(sock)
			self.objmap.pop(id(sock), None)
		except OSError as e:
			# StreamWriter.awrite() first tries to write to a socket,
			# and if that succeeds, yield IOWrite may never be called
			# for that socket, and it will never be added to poller. So,
			# ignore such error.
			if e.args[0] != uerrno.ENOENT:
				raise

	def wait(self, delay):
		# We need one-shot behavior (second arg of 1 to .poll())
		res = self.poller.ipoll(delay, 1)
		# Remove "if res" workaround after
		# https://github.com/micropython/micropython/issues/2716 fixed.
		if res:
			for sock, ev in res:
				cb = self.objmap[id(sock)]
				if ev & (select.POLLHUP | select.POLLERR):
					# These events are returned even if not requested, and
					# are sticky, i.e. will be returned again and again.
					# If the caller doesn't do proper error handling and
					# unregister this sock, we'll busy-loop on it, so we
					# as well can unregister it now "just in case".
					self.remove_reader(sock)
				if isinstance(cb, tuple):
					cb[0](*cb[1])
				else:
					cb.pend_throw(None)
					self.call_soon(cb)

class StreamReader:

	def __init__(self, polls, ios=None):
		if ios is None:
			ios = polls
		self.polls = polls
		self.ios = ios

	def read(self, n=-1):
		while True:
			yield IORead(self.polls)
			res = self.ios.read(n)
			if res is not None:
				break
			# This should not happen for real sockets, but can easily
			# happen for stream wrappers (ssl, websockets, etc.)
		if not res:
			yield IOReadDone(self.polls)
		return res

	def readexactly(self, n):
		buf = b""
		while n:
			yield IORead(self.polls)
			res = self.ios.read(n)
			assert res is not None
			if not res:
				yield IOReadDone(self.polls)
				break
			buf += res
			n -= len(res)
		return buf

	def readline(self):
		buf = b""
		while True:
			yield IORead(self.polls)
			res = self.ios.readline()
			assert res is not None
			if not res:
				yield IOReadDone(self.polls)
				break
			buf += res
			if buf[-1] == 0x0a:
				break
		return buf

	def aclose(self):
		yield IOReadDone(self.polls)
		self.ios.close()

	def __repr__(self):
		return "<StreamReader %r %r>" % (self.polls, self.ios)

class StreamWriter:

	def __init__(self, s, extra):
		self.s = s
		self.extra = extra

	def awrite(self, buf, off=0, sz=-1):
		# This method is called awrite (async write) to not proliferate
		# incompatibility with original asyncio. Unlike original asyncio
		# whose .write() method is both not a coroutine and guaranteed
		# to return immediately (which means it has to buffer all the
		# data), this method is a coroutine.
		if sz == -1:
			sz = len(buf) - off
		while True:
			res = self.s.write(buf, off, sz)
			# If we spooled everything, return immediately
			if res == sz:
				return
			if res is None:
				res = 0
			assert res < sz
			off += res
			sz -= res
			yield IOWrite(self.s)

	# Write piecewise content from iterable (usually, a generator)
	def awriteiter(self, iterable):
		for buf in iterable:
			yield from self.awrite(buf)

	def aclose(self):
		yield IOWriteDone(self.s)
		self.s.close()

	def get_extra_info(self, name, default=None):
		return self.extra.get(name, default)

	def __repr__(self):
		return "<StreamWriter %r>" % self.s

_event_loop_class = EventLoop
# print("import uasyncio successful")