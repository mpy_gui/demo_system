"""
Emulation of the io module which is needed by the abstraction layer to provide an input method when the program
is run on the computer. Later, when executed on the embedded system, the io module of the embedded system will be used
instead of this module. The IO abstraction layer is implemented in irq.py
"""

print("import mpio")
import mpywrapper as mp
import random
import time
from mpywrapper import debugprint

class Touch:
    """
    Touch class
    """
    def __init__(self):
        """
        The Touch class needs to be instantiated one time to create the binding between the gfx output on the computer
        and the touch input. Internally it uses tkinter. The object is created when the module is imported by
        mpywrapper.py
        The touch class emulates the ringbuffer by applying the internal variable "EventList" which is implemented
        on the embedded hardware in C
        """
        self.TCallbackList = []
        self.EventList  = []
        self.tthr = None
        mp.gfxObj.bind('<Motion>', self.mouseMove)
        mp.gfxObj.bind("<Button-1>", self.mousePress)
        mp.gfxObj.bind("<ButtonRelease-1>", self.mouseRelease)

    def registerTouchCallback(self, fun):
        self.TCallbackList.append(fun)
        debugprint("registered to IO TcallbackList, List content: "+ str(self.TCallbackList))

    def mouseMove(self, event):
        """
        Function which uses tkinter to get the mouse move event and inserts the event in the internally
        emulated rinbuffer by using "EventList"
        :param event: tkinter mouse move event
        """
        EVENT_MOVE = 1
        message = event.x, event.y, EVENT_MOVE
        self.EventList.insert(0,message)

    def mousePress(self, event):
        """
        Function which uses tkinter to get the mouse press event and inserts the event in the internally
        emulated rinbuffer by using "EventList"
        :param event: tkinter mouse press event
        """
        EVENT_PRESS = 2
        message = event.x, event.y, EVENT_PRESS
        self.EventList.insert(0, message)

    def mouseRelease(self, event):
        """
        Function which uses tkinter to get the mouse press event and inserts the event in the internally
        emulated rinbuffer by using "EventList"
        :param event: tkinter mouse press event
        """
        EVENT_RELEASE = 4
        message = event.x, event.y, EVENT_RELEASE
        self.EventList.insert(0, message)



#### At an earlier development stage of the project the touch inputs were simulated with random events.
#### This part was replaced by real inputs with the use of mouse events and tkinter
    '''
    async def simTouchEvents(self):
        while (True):
            print("SIM TOUCH EVENT")
            # x, y, evt
            message = (random.randint (0,480), random.randint (0,272), random.randint (0,3))
            print("Touch input x: "+str(message[0])+" y: "+str(message[1])+" evt: "+str(message[2]))
            self.EventList.insert(0,message)
            await mp.uasyncio.sleep(0.5)

    def startTouchSimulation(self):
        loop = mp.uasyncio.get_event_loop()
        loop.create_task(self.simTouchEvents())
    '''

    def execTouchCallbacks(self):
        while (len(self.EventList) > 0):
            message = self.EventList.pop()
            for fun in self.TCallbackList:
                fun(message[0], message[1], message[2])

tObj = Touch()


def registerTouchCallback(fun):
    """
    Function to register the controller callback handlers to the input event stream. The function is accessed
    through the irq module like following:
        mp.irq.registerTouchCallback(TCallback)
    when TCallback is the callback handler of the controller from which every widget callback is called

    :param fun: Callback handler function of the controller with the function header like TCallback(x, y, evt)
    x: x-coordinate of the touch event
    y: y-coordinate of the touch event
    evt: type of the touch event (EVENT_MOVE, EVENT_PRESS, EVENT_RELEASE)
    """
    tObj.registerTouchCallback(fun)


def execTouchCallbacks():
    """
    The function loops through the event list and executes the registered callback functions
    It is triggered by poll event loop which is an asyncio task
    """
    tObj.execTouchCallbacks()


def enableTouch():
    pass
    ### At the earlier development stage of the project the touch inputs were simulated
    #tObj.startTouchSimulation()

