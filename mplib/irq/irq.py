"""
Abstraction layer in order to apply the correct IO module. The embedded hardware stores the I/O events in a ringbuffer
which is defined in C. The irqPoll asyncio task polls the ringbuffer for new input events. On the development computer
the events of the input are stored into abuffer with Python.
The irq module cares, that both (computer or embedded device) buffers can be accessed in the same way.
"""


from settings import *
import mpywrapper as mp

# print("importing irq")

loop = mp.uasyncio.get_event_loop()


class IRQ():
    """
    The controller has to register the controller callback handlers to the input event stream

    e.g.:
    #defining the callback function which iterates over the view tree with the corresponding touch event
    def TCallback(x, y, evt):
        ...
        ...

    #register controller callback handler to the input event stream
    mp.irq.registerTouchCallback(TCallback)
    """
    def registerTouchCallback(self, fun):
        # print("irq registerTouchCallback")
        mp.io.registerTouchCallback(fun)

    def enableAll(self):
        mp.io.enableTouch()
        loop = mp.uasyncio.get_event_loop()
        loop.create_task(self.irqPoll())

    async def irqPoll(self):
        """
        irqPoll is called by the asyncio event loop and reads the input data of the ringbuffer
        """
        # print("irqPoll started")
        while True:
            mp.io.execTouchCallbacks()
            if (CPYTHON == True):
                await mp.uasyncio.sleep(1 / 10)
            else:
                await mp.uasyncio.sleep_ms(50)


irqObj = IRQ()


def registerTouchCallback(fun):
    irqObj.registerTouchCallback(fun)


def enableAll():
    irqObj.enableAll()
