"""
Module to implement the button widget
"""
import mpywrapper as mp
from mpywrapper import debugprint
from settings import *

BUTTON_TEXT_COLOR = "0x111111"
BUTTON_FONT = "arial"
BUTTON_FONSIZE = 20
BUTTON_BORDER_COLOR = "0x001000"
BUTTON_BACKGROUND_COLOR = "0x6a8ce2"
BUTTON_HIGHLIGHT_COLOR = "0x2f0ffa"
BUTTON_SHADOW_COLOR = "0x008be4"


class Button:
    """
    Button class
    """
    def __init__(self, text = "", Tcbfunc = None):
        """
        Initializes a button object. The dimensions are contained in the view module.
        :param text: Text which is displayed on the button
        :param Tcbfunc: Callback function which is called when the button is affected by a touch event
        """
        self.x = 0
        self.y = 0
        self.width = 0
        self.height = 0
        self.updateFlag = True
        self.text = text
        self.MCallbackList = []
        self.TCallbackList = []
        if Tcbfunc is not None:
            self.TCallbackregister(Tcbfunc)

    def MCallbackregister(self, fun):
        """
        Register a Callback function which is called when the button is affected by a model change event
        :param fun: The callback function
        """
        self.MCallbackList.append(fun)
        debugprint("registered to Button MCallback, List content: " + str(self.TCallbackList))

    def TCallbackregister(self, fun):
        """
        Register a Callback function which is called when the button is affected by a touch event
        :param fun: The callback function
        """
        self.TCallbackList.append(fun)
        debugprint("registered to Button TCallback, List content: "+str(self.TCallbackList))

    def settext(self, str):
        """
        Set the displayed text of the button
        :param fun: The callback function
        """
        self.text = str;

    def draw(self, fill = True):
        """
        The draw function to render the button on the screen
        :param fill: True (default) if the area has to be cleared before redrawing
        """
        if fill == True:
            mp.gfx.set_color(mp.color_convert(BUTTON_BACKGROUND_COLOR))
            mp.fill_rectangle(self.x , self.y , self.width , self.height)
        mp.gfx.set_color(mp.color_convert(BUTTON_BORDER_COLOR))
        mp.gfx.draw_rectangle(self.x, self.y, self.width, self.height)
        mp.writer.setfont(BUTTON_FONT,BUTTON_FONSIZE)
        # button shadow effect
        mp.gfx.set_color(mp.color_convert(BUTTON_SHADOW_COLOR))
        for thickness in range(1,4):
            mp.gfx.draw_line(self.x + thickness, self.y+ self.height-thickness, self.x+ self.width-thickness, self.y+self.height-thickness)
            mp.gfx.draw_line(self.x + self.width - thickness, self.y + thickness, self.x + self.width - thickness, self.y + self.height - thickness)
        # text shadow
        mp.writer.putstr(self.text, self.x + 9, self.y + 7)
        mp.writer.putstr(self.text, self.x + 10, self.y + 8)
        #foreground text
        mp.gfx.set_color(mp.color_convert(BUTTON_TEXT_COLOR))
        mp.writer.putstr(self.text, self.x + 8, self.y + 6)

    def pressed(self):
        """
        Callback function which can be registered with a touch press event on the button. Use TCallbackregister for
        registering the function to the button.
        """
        mp.gfx.set_color(mp.color_convert(BUTTON_HIGHLIGHT_COLOR))
        mp.gfx.draw_rectangle(self.x, self.y, self.width, self.height)
        mp.writer.setfont(BUTTON_FONT, BUTTON_FONSIZE)
        mp.writer.putstr(self.text, self.x + 9, self.y + 7)
        mp.writer.putstr(self.text, self.x + 8, self.y + 6)

    def released(self):
        """
         Callback function which can be registered with a touch release event on the button. Use TCallbackregister for
         registering the function to the button.
         """
        self.draw(False)