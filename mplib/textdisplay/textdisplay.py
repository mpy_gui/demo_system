"""
Module to implement the textbox widget. The textbox widget is used to display text.
"""
import mpywrapper as mp
from mpywrapper import debugprint
from settings import *


TEXTDISPLAY_BACKCOLOR = "0xFFFFFF"
TEXTDISPLAY_TEXTCOLOR = "0x401000"
TEXTDISPLAY_FONT = "courier"
TEXTDISPLAY_FONTSIZE = 10


class Textdisplay:
    """
    Textdisplay class
    """
    def __init__(self, text = ""):
        """
        Initializes a textbox object. The dimensions are contained in the view module.
        :param text: Text which is displayed in the box
        """
        self.x = 0
        self.y = 0
        self.width = 0
        self.height = 0
        self.updateFlag = True
        self.text = text
        self.MCallbackList = []
        self.TCallbackList = []
        self.chr_xpos = 15
        self.chr_ypos = 10

    def MCallbackregister(self, fun):
        """
        Register a Callback function which is called when the textbox is affected by a model change event
        :param fun: The callback function
        """
        self.MCallbackList.append(fun)
        debugprint("registered to Canvas MCallback, List content: " + str(self.TCallbackList))

    def settext(self, str):
        """
        Set the text of the textbox
        :param str: The string which will be shown in the textbox
        """
        self.text = str;

    def draw(self):
        """
        The draw function to render the textbox on the screen
        The function is customized for the demo system
        """
        mp.gfx.set_color(mp.color_convert(TEXTDISPLAY_BACKCOLOR))
        mp.fill_rectangle(self.x+1, self.y+1, self.width-2, self.height-2)
        outtext = self.text
        mp.gfx.set_color(mp.color_convert(TEXTDISPLAY_TEXTCOLOR))
        mp.gfx.draw_rectangle(self.x, self.y, self.width, self.height)
        mp.writer.setfont(TEXTDISPLAY_FONT,TEXTDISPLAY_FONTSIZE)

        # get the last lines of the model data
        st = self.text.split('\n')
        if '' in st:
            st.remove('')
        st.reverse()
        output =[]
        try:
            for i in range(0,3):
                output.append(st[i])
        except IndexError:
            pass
        # write the last lines to the display area
        output.reverse()
        for i in range (len(output)):
            self.chr_ypos = 10 + i*20
            self.chr_xpos = 15
            for c in output[i]:
                if "Error" in output[i]:
                    mp.gfx.set_color(mp.color_convert("0xFF0000"))
                else:
                    mp.gfx.set_color(mp.color_convert(TEXTDISPLAY_TEXTCOLOR))
                if (c != chr(10)):
                    self.chr_xpos = mp.writer.putc(c, self.chr_xpos, self.chr_ypos) + 1