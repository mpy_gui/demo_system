"""
Module to implement the canvas widget. The canvas widget is used to group widgets together.
"""
import mpywrapper as mp
from mpywrapper import debugprint
from settings import *

class Canvas:
    """
    Canvas class
    """
    def __init__(self):
        """
        Initializes a canvas object. The dimensions are contained in the view module.
        """
        self.x = 0
        self.y = 0
        self.width = 0
        self.height = 0
        self.updateFlag = True
        self.MCallbackList = []
        self.TCallbackList = []

    def MCallbackregister(self, fun):
        """
        Register a Callback function which is called when the widget is affected by a model change event
        :param fun: The callback function
        """
        self.MCallbackList.append(fun)
        debugprint("registered to Canvas MCallback, List content: " + str(self.TCallbackList))

    def TCallbackregister(self, fun):
        """
        Register a Callback function which is called when the widget is affected by a touch event
        :param fun: The callback function
        """
        self.TCallbackList.append(fun)
        debugprint("registered to Canvas TCallback, List content: "+str(self.TCallbackList))

    def draw(self):
        """
        Dummy draw function to provide the interface to the view module
        """
        pass
