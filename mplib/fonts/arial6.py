# arial6.py 
# Generated by micropython font creator

# (ASCII) : (width), (height), [bitmask]
characters = {
    32: (3, 1, [0,0,0]),
    33: (2, 4, [1,1,1,1,1,1,1,1]),
    34: (3, 1, [1,1,1]),
    35: (5, 4, [0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,0]),
    36: (4, 6, [0,1,1,0,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,0]),
    37: (6, 4, [1,1,1,1,1,0,1,1,1,1,0,0,1,1,1,1,1,1,0,1,1,1,1,1]),
    38: (4, 4, [1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,1]),
    39: (2, 1, [1,1]),
    40: (2, 5, [1,1,1,1,1,1,1,1,1,1]),
    41: (3, 5, [1,1,0,1,1,0,1,1,1,1,1,0,1,1,0]),
    42: (4, 1, [1,1,1,1]),
    43: (4, 3, [0,1,1,0,0,1,1,0,1,1,1,1]),
    44: (2, 2, [1,1,1,1]),
    45: (4, 1, [1,1,1,1]),
    46: (2, 1, [1,1]),
    47: (3, 4, [0,1,1,0,1,1,0,1,1,1,1,0]),
    48: (4, 4, [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0]),
    49: (3, 4, [1,1,1,1,1,1,0,1,1,0,1,1]),
    50: (4, 4, [1,1,1,1,0,1,1,1,1,1,1,0,1,1,1,1]),
    51: (4, 4, [1,1,1,1,0,1,1,0,0,1,1,1,1,1,1,1]),
    52: (5, 4, [0,0,1,1,0,0,1,1,1,0,1,1,1,1,1,0,0,1,1,0]),
    53: (4, 4, [1,1,1,0,1,1,1,0,0,0,1,1,1,1,1,1]),
    54: (5, 4, [0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1]),
    55: (4, 4, [1,1,1,1,0,1,1,0,1,1,0,0,1,1,0,0]),
    56: (4, 4, [1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1]),
    57: (4, 4, [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0]),
    58: (2, 3, [1,1,0,0,1,1]),
    59: (2, 4, [1,1,0,0,1,1,1,1]),
    60: (4, 3, [0,0,1,1,1,1,1,1,1,1,1,1]),
    61: (4, 2, [1,1,1,1,1,1,1,1]),
    62: (4, 3, [1,0,0,0,1,1,1,1,1,1,1,1]),
    63: (4, 4, [1,1,1,1,0,1,1,1,0,1,1,0,0,1,1,0]),
    64: (6, 5, [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]),
    65: (6, 4, [0,0,1,1,0,0,0,1,1,1,1,0,0,1,1,1,1,0,1,1,1,1,1,1]),
    66: (4, 4, [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]),
    67: (5, 4, [1,1,1,1,1,1,1,0,0,0,1,1,0,0,0,1,1,1,1,1]),
    68: (5, 4, [1,1,1,1,0,1,1,0,1,1,1,1,0,1,1,1,1,1,1,0]),
    69: (4, 4, [1,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1]),
    70: (4, 4, [1,1,1,1,1,1,0,0,1,1,1,1,1,1,0,0]),
    71: (5, 4, [1,1,1,1,1,1,1,0,0,0,1,1,1,1,1,1,1,1,1,1]),
    72: (5, 4, [1,1,0,1,1,1,1,1,1,1,1,1,0,1,1,1,1,0,1,1]),
    73: (2, 4, [1,1,1,1,1,1,1,1]),
    74: (3, 4, [0,1,1,0,1,1,0,1,1,1,1,1]),
    75: (5, 4, [1,1,1,1,0,1,1,1,0,0,1,1,1,1,0,1,1,1,1,1]),
    76: (4, 4, [1,1,0,0,1,1,0,0,1,1,0,0,1,1,1,1]),
    77: (5, 4, [1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]),
    78: (5, 4, [1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]),
    79: (5, 4, [1,1,1,1,1,1,1,0,1,1,1,1,0,1,1,1,1,1,1,1]),
    80: (4, 4, [1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0]),
    81: (5, 4, [1,1,1,1,1,1,1,0,1,1,1,1,0,1,1,1,1,1,1,1]),
    82: (5, 4, [1,1,1,1,1,1,1,1,1,0,1,1,1,1,0,1,1,1,1,1]),
    83: (4, 4, [1,1,1,1,1,1,1,0,0,1,1,1,1,1,1,1]),
    84: (4, 4, [1,1,1,1,0,1,1,0,0,1,1,0,0,1,1,0]),
    85: (5, 4, [1,1,0,1,1,1,1,0,1,1,1,1,0,1,1,1,1,1,1,0]),
    86: (6, 4, [1,1,1,1,1,1,0,1,1,1,1,0,0,1,1,1,1,0,0,0,1,1,0,0]),
    87: (7, 4, [1,1,1,1,1,1,1,0,1,1,1,1,1,1,0,1,1,1,1,1,1,0,1,1,1,1,1,0]),
    88: (6, 4, [0,1,1,1,1,0,0,0,1,1,0,0,0,1,1,1,1,0,1,1,1,1,1,1]),
    89: (6, 4, [1,1,1,1,1,1,0,1,1,1,1,0,0,0,1,1,0,0,0,0,1,1,0,0]),
    90: (5, 4, [0,1,1,1,1,0,0,1,1,0,0,1,1,0,0,1,1,1,1,1]),
    91: (2, 5, [1,1,1,1,1,1,1,1,1,1]),
    92: (3, 4, [1,1,0,0,1,1,0,1,1,0,1,1]),
    93: (2, 5, [1,1,1,1,1,1,1,1,1,1]),
    94: (3, 2, [1,1,1,1,1,1]),
    95: (5, 1, [1,1,1,1,1]),
    96: (2, 1, [1,1]),
    97: (4, 3, [1,1,1,1,1,1,1,1,1,1,1,1]),
    98: (4, 4, [1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1]),
    99: (5, 3, [0,1,1,1,1,1,1,1,0,0,0,1,1,1,1]),
    100: (5, 4, [0,0,0,1,1,0,1,1,1,1,1,1,1,1,1,0,1,1,1,1]),
    101: (5, 3, [0,1,1,1,1,1,1,1,1,1,0,1,1,1,1]),
    102: (3, 4, [0,1,1,1,1,1,0,1,1,0,1,1]),
    103: (5, 4, [0,1,1,1,1,1,1,0,1,1,0,1,1,1,1,0,1,1,1,1]),
    104: (4, 4, [1,1,0,0,1,1,1,1,1,1,1,1,1,1,1,1]),
    105: (2, 4, [1,1,1,1,1,1,1,1]),
    106: (3, 5, [0,1,1,0,1,1,0,1,1,0,1,1,1,1,1]),
    107: (4, 4, [1,1,0,0,1,1,1,0,1,1,1,0,1,1,1,1]),
    108: (2, 4, [1,1,1,1,1,1,1,1]),
    109: (5, 3, [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]),
    110: (4, 3, [1,1,1,1,1,1,1,1,1,1,1,1]),
    111: (5, 3, [0,1,1,1,1,1,1,0,1,1,0,1,1,1,1]),
    112: (4, 4, [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0]),
    113: (5, 4, [0,1,1,1,1,1,1,1,1,1,0,1,1,1,1,0,1,1,1,1]),
    114: (3, 3, [1,1,1,1,1,0,1,1,0]),
    115: (3, 3, [1,1,1,1,1,1,1,1,1]),
    116: (2, 4, [1,1,1,1,1,1,1,1]),
    117: (4, 3, [1,1,1,1,1,1,1,1,1,1,1,1]),
    118: (5, 3, [1,1,1,1,1,0,1,1,1,0,0,1,1,1,0]),
    119: (6, 3, [1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,0]),
    120: (5, 3, [0,1,1,1,0,0,1,1,1,0,1,1,1,1,1]),
    121: (4, 4, [1,1,1,1,1,1,1,0,1,1,1,0,1,1,0,0]),
    122: (4, 3, [0,1,1,1,0,1,1,0,1,1,1,1]),
    123: (3, 5, [0,1,1,0,1,1,1,1,1,0,1,1,0,1,1]),
    124: (2, 5, [1,1,1,1,1,1,1,1,1,1]),
    125: (3, 5, [1,1,0,1,1,0,1,1,1,1,1,0,1,1,0]),
    126: (4, 1, [1,1,1,1]),
}
