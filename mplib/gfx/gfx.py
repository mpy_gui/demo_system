"""
The gfx Python module is used on the development computer to allow the displaying of the content
which is normally rendered to the LCD screen of the embedded device. It is called from the mpywrapper module.
"""

print("importing gfx")
import mpywrapper as mp
import tkinter as tk
from random import randint
# https://stackoverflow.com/questions/47895765/use-asyncio-and-tkinter-together-without-freezing-the-gui


class GFXSim(tk.Tk):
    """
    The GFXSim class implements uses tkinter to render the contents to the display
    """
    def __init__(self):
        """
        One instance of a GFXSim object has to be created to add the display task to the asyncio event loop
        """
        super().__init__()
        self.loop = mp.uasyncio.get_event_loop()
        self.protocol("WM_DELETE_WINDOW", self.close)
        self.loop.create_task(self.display())
        self.canvas = tk.Canvas(self, height=272, width=480, bg = 'white')
        self.canvas.pack()
        self.color = "black"

    def close(self):
        self.loop.stop()
        self.destroy()

    async def display(self):
        interval = 1 / 120
        while True:
            self.update()
            await mp.uasyncio.sleep(interval)

    def set_color(self, val):
        self.color = val

    def get_color(self):
        return self.color

    def drawPixel(self, x,y):
        x = x+2
        y = y+2
        self.canvas.create_line(x, y, x+1, y+1, fill=self.color)

# create singleton
mp.gfxObj = GFXSim()

# module functions
def draw_pixel(x,y):
    mp.gfxObj.drawPixel(x,y)

def randint(fr, to):
    return (randint(fr, to))

def draw_line(x1, y1, x2, y2):
    # Harmony: cpuDrawLine_Normal
    dx = x2- x1;
    if (dx < 0):
        dx *= -1
    sx = 1 if (x1 < x2) else -1
    dy = y2 - y1;
    if (dy < 0):
        dy *= -1
    dy *= -1
    sy = 1 if (y1 < y2) else -1
    err = dx + dy

    while True:
        draw_pixel(x1,y1)
        if (x1 == x2 and y1 == y2):
            break
        e2 = 2 * err
        if (e2 >= dy):
            err += dy
            x1 += sx

        if (e2 <= dx):
                err += dx
                y1 += sy

def draw_rectangle(x, y, width, height):
    #top
    draw_line(x, y, x + width, y)
    #bottom
    draw_line(x, y + height, x + width, y + height)
    #left
    draw_line(x, y, x, y + height)
    #right
    draw_line(x+width, y, x+width, y+height)

def set_color(val):
    mp.gfxObj.set_color(val)

def get_color():
    return mp.gfxObj.get_color()

def draw_rectangle_fill(x,y,width,height):
    for row in range (x,x+width):
        for col in range (y, y+height):
            mp.gfx.draw_pixel(row,col)