"""
This module is needed for the abstraction of the hardware platform (computer or embedded device).
Its main task is to add the platform dependent paths to the sys.path variable
correctly and to administrate the imports. To distinguish which system is used, there is a global
file (settings.py) which contains a variable. The variable is set differently on the hardware and on the computer
and the settings file is kept every time on the system. Now the other files which
need to import a Python source, import via the wrapper the correct platform dependent file like following:

import mpywrapper as mp
loop = mp. asyncio . get_event_loop ()
"""

import sys
from settings import *


def init():
    """
    The init function has to be called at the setup of the GUI (in the controller) before starting the asyncio loop.
    It sets the paths correctly and imports the correct platform dependent modules.
    """
    if (CPYTHON == True):
        print("Init for use with pycharm...")
        import os
        dirpath = os.path.dirname(__file__)
        sys.path.append(os.path.join(dirpath, 'uerrno'))
        sys.path.append(os.path.join(dirpath, 'irq'))
        sys.path.append(os.path.join(dirpath, 'gfx'))
        sys.path.append(os.path.join(dirpath, 'mpio'))
        sys.path.append(os.path.join(dirpath, 'writer'))
        sys.path.append(os.path.join(dirpath, 'fonts'))
        sys.path.append(os.path.join(dirpath, 'button'))
        sys.path.append(os.path.join(dirpath, 'canvas'))
        sys.path.append(os.path.join(dirpath, 'textdisplay'))
        global uasyncio
        global irq
        global button
        global canvas
        global textdisplay
        global view
        global model
        global uerrno
        global gfx
        global io
        global gfxObj
        global writer
        global externalmodelmodifier
        import uerrno
        import asyncio as uasyncio
        import view
        import irq
        import gfx
        import mpio as io
        import writer
        import button
        import canvas
        import textdisplay
        import model
        import externalmodelmodifier
    else:
        print("Init for use on hardware...")
        sys.path.clear()
        sys.path.append("/sd")
        sys.path.append("/sd/mplib")
        sys.path.append("/sd/mplib/uerrno")
        sys.path.append("/sd/mplib/view")
        sys.path.append("/sd/mplib/uasyncio")
        sys.path.append("/sd/mplib/irq")
        sys.path.append("/sd/mplib/button")
        sys.path.append("/sd/mplib/canvas")
        sys.path.append("/sd/mplib/writer")
        sys.path.append("/sd/mplib/fonts")
        sys.path.append("/sd/mplib/textdisplay")
        global uasyncio
        global irq
        global button
        global canvas
        global textdisplay
        global view
        global model
        global uerrno
        global gfx
        global io
        global writer
        import textdisplay
        import uerrno
        import uasyncio
        import view
        import irq
        import gfx
        import io
        import writer
        import button
        import canvas
        import model

def color_convert(hex_rrggbb):
    """
    Function to convert colors to the specific format needed for the target hardware
    :param hex_rrggbb: RGB hexadecimal value
    :return: platform specific format of a color value which can be processed by the platform dependent gfx module
    """
    if (CPYTHON == True):
        ret = str(hex_rrggbb)
        ret = ret[2:]
        ret = "#"+ret
        return ret
    else:
        ret = str(hex_rrggbb)
        ret = ret[2:]
        return int(ret,16)

def debugprint(line):
    """
    Function to print debug messages
    :param line: line which has to be printed
    """
    if DEBUGPRINT == True:
        print(line)

def fill_rectangle(x,y,width,height):
    """
    Function to fill a rectangle with the last set color
    :param x: left x coordinate of the rectangle
    :param y: upper y coordinate of the rectangle
    :param width: width of the rectangle
    :param height:  height of the rectangle
    """
    gfx.draw_rectangle_fill(x, y, width, height)