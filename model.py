"""
The model is representing the data of the application according to the MVC-Pattern.
At model changes the registered listeners are called with the notify function.
"""
import mpywrapper as mp
# print("importing model")
from settings import *

if CPYTHON == False:
    import demo
    import uasyncio

class Model:
    def __init__(self):
        """
        The model of the demo system consists of a text
        On the embedded hardware, the C-Module demo.c is included into the project
        """
        self.text = ""
        self.MCallbackList = []
        # update_pending is inserted for performance optimization
        # textdisplay gets only refreshed if a character on the
        # lcd console is typed, or when a complete line is received
        self.update_pending = True
        if CPYTHON == False:
            demo.init()
            self._add_to_eventloop()

    def putc(self, ch):
        """
        Add a character to the text
        :param ch: character
        """
        if CPYTHON == False:
            #accessing the demo.c module
            demo.putc(ord(ch))
            self.update_pending = True
        else:
            self.text = self.text + ch
            self.notify()

    def delc(self):
        """
        Delete a character from the text
        :param ch: character
        """
        if CPYTHON == False:
            #accessing the demo.c module
            demo.delc()
            self.update_pending = True
        self.text = self.text[:-1]
        self.notify()

    def parse_compile_execute(self):
        if (CPYTHON == False):
            #call the python interpreter from demo.c
            demo.parse_compile_execute()
            self.update_pending = False
        else:
            self.text = ""
            self.notify()

    def notify(self):
        """
        Notifies the listeners to model changes
        :param self: The model which changes
        """
        mp.debugprint("Model changed, notifiy Listeners")
        self.update_pending = False
        for element in self.MCallbackList:
            element(self.text)

    def MCallbackregister(self, fun):
        """
        Register a callback function which reacts to model changes.
        :param fun: The callback function
        :param self: The model which changes
        """
        self.MCallbackList.append(fun)
        mp.debugprint("registered to Model MCallbacklist, List content: " + str(self.MCallbackList))

    def _add_to_eventloop(self):
        # add the model to the asyncio loop
        global uasyncio
        loop = uasyncio.get_event_loop()
        loop.create_task(self.pollCModel())

    async def pollCModel(self):
        """
        Poll the model for changes. Called by asyncio event loop
        """
        while (True):
            tmp =  demo.getc()
            #printable character
            if (tmp >= 32 and tmp <= 126):
                self.text = self.text + chr(tmp)
                if (tmp == 62):
                    # promt '>' character
                    self.update_pending = True
            if (tmp == 10):
                #CR
                self.update_pending = True
                self.text = self.text + chr(tmp)
            #element was in buffer
            if (tmp != -1 and self.update_pending == True):
                self.notify()
            await uasyncio.sleep_ms(20)
