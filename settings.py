"""
The settings file contains the platform specific definitions and remains every time in the project folder on the platform
(computer or embedded hardware). It is mandatory to have the right settings.py file to make the execution of the program
possible on both platforms
"""

# The CPYTHON variable determines if the program is run on the computer (CPYTHON = True) or on the
# embedded hardware (CPYTHON = False)
CPYTHON = True
DEBUGPRINT = False

# microchip event constants
EVENT_MOVE = 1
EVENT_PRESS = 2
EVENT_STILLPRESS = 3
EVENT_RELEASE = 4
