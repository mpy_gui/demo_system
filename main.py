"""
This module represents the controller of the MVC Pattern which controls the communication between View and Model
"""


#################################### PROGRAMM INITIALIZATION ###########################################################
import builtins
import sys
sys.path.append("/sd")
from settings import *
#import externalmodelmodifier

if CPYTHON:
    import os
    sys.path.append(os.path.join(os.path.dirname(__file__), 'mplib'))
else:
    sys.path.append("/sd/mplib")
import mpywrapper as mp

mp.init()
m = mp.model.Model()
# e = mp.externalmodelmodifier.EModify(m)

#################################### WIDGET DEFINITIONS  ###############################################################

##### root node (display area) #####
root = mp.view.Node(mp.canvas.Canvas(),0,0,480,272)

##### text area ####################
# text area model callback
def textdisplayMCallback(text):
    textdisplay.settext(text)
# text area widget
textdisplay = mp.textdisplay.Textdisplay(m.text)
textdisplay.MCallbackregister(textdisplayMCallback)
root.add_child(textdisplay,5,5,470,70)

##### keyboard ####################
# keyboard touch callbacks
def keyboard_char_tcb(key, x, y, evt):
    if evt == EVENT_PRESS:
        key.pressed()
    if evt == EVENT_RELEASE:
        m.putc(key.text)
        key.released()

def keyboard_clean():
    root.remove_tree(row1a)
    root.remove_tree(row2a)
    root.remove_tree(row3a)
    root.remove_tree(row4a)
    root.remove_tree(row1b)
    root.remove_tree(row2b)
    root.remove_tree(row3b)
    root.remove_tree(row4b)
    root.remove_tree(row1c)
    root.remove_tree(row2c)
    root.remove_tree(row3c)
    root.remove_tree(row4c)

def keyboard_to_lower_tcb(key,x,y,evt):
    if evt == EVENT_PRESS:
        key.pressed()
    if (evt == EVENT_RELEASE):
        keyboard_clean()
        root.append_tree(row1b)
        root.append_tree(row2b)
        root.append_tree(row3b)
        root.append_tree(row4b)

def keyboard_to_upper_tcb(key,x,y,evt):
    if evt == EVENT_PRESS:
        key.pressed()
    if (evt == EVENT_RELEASE):
        keyboard_clean()
        root.append_tree(row1a)
        root.append_tree(row2a)
        root.append_tree(row3a)
        root.append_tree(row4a)

def keyboard_del_tcb(key,x,y,evt):
    if evt == EVENT_PRESS:
        key.pressed()
    if (evt == EVENT_RELEASE):
        m.delc()
        key.released()

def keyboard_enter_tcb(key, x,y,evt):
    if evt == EVENT_PRESS:
        key.pressed()
    if (evt == EVENT_RELEASE):
        m.parse_compile_execute()
        key.released()

def keyboard_to_symbols_tcb(key,x,y,evt):
    if evt == EVENT_PRESS:
        key.pressed()
    if (evt == EVENT_RELEASE):
        keyboard_clean()
        root.append_tree(row1c)
        root.append_tree(row2c)
        root.append_tree(row3c)
        root.append_tree(row4c)

# keyboard uppercase
row1a = root.add_child(mp.canvas.Canvas(),0,80,480,48)
row1a.add_child(mp.button.Button("Q", keyboard_char_tcb), 0,0,48,48)
row1a.add_child(mp.button.Button("W", keyboard_char_tcb), 48,0,48,48)
row1a.add_child(mp.button.Button("E", keyboard_char_tcb), 96,0,48,48)
row1a.add_child(mp.button.Button("R", keyboard_char_tcb), 144,0,48,48)
row1a.add_child(mp.button.Button("T", keyboard_char_tcb), 192,0,48,48)
row1a.add_child(mp.button.Button("Z", keyboard_char_tcb), 240,0,48,48)
row1a.add_child(mp.button.Button("U", keyboard_char_tcb), 288,0,48,48)
row1a.add_child(mp.button.Button("I", keyboard_char_tcb), 336,0,48,48)
row1a.add_child(mp.button.Button("O", keyboard_char_tcb), 384,0,48,48)
row1a.add_child(mp.button.Button("P", keyboard_char_tcb), 432,0,48,48)
row2a = root.add_child(mp.canvas.Canvas(),0,48+80,480,48)
row2a.add_child(mp.button.Button("A", keyboard_char_tcb), 24,0,48,48)
row2a.add_child(mp.button.Button("S", keyboard_char_tcb), 72,0,48,48)
row2a.add_child(mp.button.Button("D", keyboard_char_tcb), 120,0,48,48)
row2a.add_child(mp.button.Button("F", keyboard_char_tcb), 168,0,48,48)
row2a.add_child(mp.button.Button("G", keyboard_char_tcb), 216,0,48,48)
row2a.add_child(mp.button.Button("H", keyboard_char_tcb), 264,0,48,48)
row2a.add_child(mp.button.Button("J", keyboard_char_tcb), 312,0,48,48)
row2a.add_child(mp.button.Button("K", keyboard_char_tcb), 360,0,48,48)
row2a.add_child(mp.button.Button("L", keyboard_char_tcb), 408,0,48,48)
row3a = root.add_child(mp.canvas.Canvas(),0,96+80,480,48)
row3a.add_child(mp.button.Button("Shift", keyboard_to_lower_tcb), 0,0,72,48)
row3a.add_child(mp.button.Button("Y", keyboard_char_tcb), 72,0,48,48)
row3a.add_child(mp.button.Button("X", keyboard_char_tcb), 120,0,48,48)
row3a.add_child(mp.button.Button("C", keyboard_char_tcb), 168,0,48,48)
row3a.add_child(mp.button.Button("V", keyboard_char_tcb), 216,0,48,48)
row3a.add_child(mp.button.Button("B", keyboard_char_tcb), 264,0,48,48)
row3a.add_child(mp.button.Button("N", keyboard_char_tcb), 312,0,48,48)
row3a.add_child(mp.button.Button("M", keyboard_char_tcb), 360,0,48,48)
row3a.add_child(mp.button.Button("Del", keyboard_del_tcb), 408,0,72,48)
row4a = root.add_child(mp.canvas.Canvas(),0,144+80,480,48)
row4a.add_child(mp.button.Button("Sym",keyboard_to_symbols_tcb), 0,0,72,48)
row4a.add_child(mp.button.Button(",", keyboard_char_tcb), 72,0,48,48)
row4a.add_child(mp.button.Button(" ", keyboard_char_tcb), 120,0,240,48)
row4a.add_child(mp.button.Button(".", keyboard_char_tcb), 360,0,48,48)
row4a.add_child(mp.button.Button("Enter", keyboard_enter_tcb), 408,0,72,48)

# keyboard lowercase
row1b = root.add_child(mp.canvas.Canvas(),0,80,480,48)
row1b.add_child(mp.button.Button("q", keyboard_char_tcb), 0,0,48,48)
row1b.add_child(mp.button.Button("w", keyboard_char_tcb), 48,0,48,48)
row1b.add_child(mp.button.Button("e", keyboard_char_tcb), 96,0,48,48)
row1b.add_child(mp.button.Button("r", keyboard_char_tcb), 144,0,48,48)
row1b.add_child(mp.button.Button("t", keyboard_char_tcb), 192,0,48,48)
row1b.add_child(mp.button.Button("z", keyboard_char_tcb), 240,0,48,48)
row1b.add_child(mp.button.Button("u", keyboard_char_tcb), 288,0,48,48)
row1b.add_child(mp.button.Button("i", keyboard_char_tcb), 336,0,48,48)
row1b.add_child(mp.button.Button("o", keyboard_char_tcb), 384,0,48,48)
row1b.add_child(mp.button.Button("p", keyboard_char_tcb), 432,0,48,48)
row2b = root.add_child(mp.canvas.Canvas(),0,48+80,480,48)
row2b.add_child(mp.button.Button("a", keyboard_char_tcb), 24,0,48,48)
row2b.add_child(mp.button.Button("s", keyboard_char_tcb), 72,0,48,48)
row2b.add_child(mp.button.Button("d", keyboard_char_tcb), 120,0,48,48)
row2b.add_child(mp.button.Button("f", keyboard_char_tcb), 168,0,48,48)
row2b.add_child(mp.button.Button("g", keyboard_char_tcb), 216,0,48,48)
row2b.add_child(mp.button.Button("h", keyboard_char_tcb), 264,0,48,48)
row2b.add_child(mp.button.Button("j", keyboard_char_tcb), 312,0,48,48)
row2b.add_child(mp.button.Button("k", keyboard_char_tcb), 360,0,48,48)
row2b.add_child(mp.button.Button("l", keyboard_char_tcb), 408,0,48,48)
row3b = root.add_child(mp.canvas.Canvas(),0,96+80,480,48)
row3b.add_child(mp.button.Button("Shift",keyboard_to_upper_tcb), 0,0,72,48)
row3b.add_child(mp.button.Button("y", keyboard_char_tcb), 72,0,48,48)
row3b.add_child(mp.button.Button("x", keyboard_char_tcb), 120,0,48,48)
row3b.add_child(mp.button.Button("c", keyboard_char_tcb), 168,0,48,48)
row3b.add_child(mp.button.Button("v", keyboard_char_tcb), 216,0,48,48)
row3b.add_child(mp.button.Button("b", keyboard_char_tcb), 264,0,48,48)
row3b.add_child(mp.button.Button("n", keyboard_char_tcb), 312,0,48,48)
row3b.add_child(mp.button.Button("m", keyboard_char_tcb), 360,0,48,48)
row3b.add_child(mp.button.Button("Del",keyboard_del_tcb), 408,0,72,48)
row4b = root.add_child(mp.canvas.Canvas(),0,144+80,480,48)
row4b.add_child(mp.button.Button("Sym",keyboard_to_symbols_tcb), 0,0,72,48)
row4b.add_child(mp.button.Button(",", keyboard_char_tcb), 72,0,48,48)
row4b.add_child(mp.button.Button(" ", keyboard_char_tcb), 120,0,240,48)
row4b.add_child(mp.button.Button(".", keyboard_char_tcb), 360,0,48,48)
row4b.add_child(mp.button.Button("Enter", keyboard_enter_tcb), 408,0,72,48)

# keyboard symbols
row1c = root.add_child(mp.canvas.Canvas(),0,80,480,48)
row1c.add_child(mp.button.Button("1", keyboard_char_tcb), 0,0,48,48)
row1c.add_child(mp.button.Button("2", keyboard_char_tcb), 48,0,48,48)
row1c.add_child(mp.button.Button("3", keyboard_char_tcb), 96,0,48,48)
row1c.add_child(mp.button.Button("4", keyboard_char_tcb), 144,0,48,48)
row1c.add_child(mp.button.Button("5", keyboard_char_tcb), 192,0,48,48)
row1c.add_child(mp.button.Button("6", keyboard_char_tcb), 240,0,48,48)
row1c.add_child(mp.button.Button("7", keyboard_char_tcb), 288,0,48,48)
row1c.add_child(mp.button.Button("8", keyboard_char_tcb), 336,0,48,48)
row1c.add_child(mp.button.Button("9", keyboard_char_tcb), 384,0,48,48)
row1c.add_child(mp.button.Button("0", keyboard_char_tcb), 432,0,48,48)

row2c = root.add_child(mp.canvas.Canvas(),0,48+80,480,48)
row2c.add_child(mp.button.Button("@", keyboard_char_tcb), 0,0,48,48)
row2c.add_child(mp.button.Button("#", keyboard_char_tcb), 48,0,48,48)
row2c.add_child(mp.button.Button("_", keyboard_char_tcb), 96,0,48,48)
row2c.add_child(mp.button.Button("&", keyboard_char_tcb), 144,0,48,48)
row2c.add_child(mp.button.Button("-", keyboard_char_tcb), 192,0,48,48)
row2c.add_child(mp.button.Button("+", keyboard_char_tcb), 240,0,48,48)
row2c.add_child(mp.button.Button("(", keyboard_char_tcb), 288,0,48,48)
row2c.add_child(mp.button.Button(")", keyboard_char_tcb), 336,0,48,48)
row2c.add_child(mp.button.Button("<", keyboard_char_tcb), 384,0,48,48)
row2c.add_child(mp.button.Button(">", keyboard_char_tcb), 432,0,48,48)

row3c = root.add_child(mp.canvas.Canvas(),0,96+80,480,48)
row3c.add_child(mp.button.Button("\"", keyboard_char_tcb), 0,0,48,48)
row3c.add_child(mp.button.Button("'", keyboard_char_tcb),48,0,48,48)
row3c.add_child(mp.button.Button(":", keyboard_char_tcb), 96,0,48,48)
row3c.add_child(mp.button.Button(";", keyboard_char_tcb), 144,0,48,48)
row3c.add_child(mp.button.Button("!", keyboard_char_tcb), 192,0,48,48)
row3c.add_child(mp.button.Button("?", keyboard_char_tcb), 240,0,48,48)
row3c.add_child(mp.button.Button("{", keyboard_char_tcb), 288,0,48,48)
row3c.add_child(mp.button.Button("}", keyboard_char_tcb), 336,0,48,48)
row3c.add_child(mp.button.Button("[", keyboard_char_tcb), 384,0,48,48)
row3c.add_child(mp.button.Button("]", keyboard_char_tcb), 432,0,48,48)

row4c = root.add_child(mp.canvas.Canvas(),0,144+80,480,48)
row4c.add_child(mp.button.Button("abc", keyboard_to_lower_tcb), 0,0,72,48)
row4c.add_child(mp.button.Button("~", keyboard_char_tcb), 72,0,48,48)
row4c.add_child(mp.button.Button("/", keyboard_char_tcb), 120,0,48,48)
row4c.add_child(mp.button.Button("*", keyboard_char_tcb), 168,0,48,48)
row4c.add_child(mp.button.Button(" ", keyboard_char_tcb), 216,0,144,48)
row4c.add_child(mp.button.Button("=", keyboard_char_tcb), 360,0,48,48)
row4c.add_child(mp.button.Button("Enter", keyboard_enter_tcb), 408,0,72,48)

# activate keyboard
keyboard_to_lower_tcb(None,0,0,EVENT_RELEASE)
root.set_update_request()


#################### REGISTER ALL WIDGET CALLBACKS TO THE CALLBACK HANDLERS OF THE CONTROLLER ##########################
# Model change callback
def MCallback(text):
    # iterating over the tree to find widgets which are registered to model changes
    for el in root:
        if (el.widget != None):
            # call all model callback functions of the widget
            for f in el.widget.MCallbackList:
                f(text)
                el.set_update_request()

# Touch input callback
def TCallback(x, y, evt):
    if ((evt == 2) or (evt == 4)):
        # iterating over the view tree to find touched widget
        for el in root.get_callback_widget(x,y):
            for f in el.widget.TCallbackList:
                # passes the affected widget, the touch x/y coordinates and touch event
                # to the touch callback functions of the widget
                f(el.widget,x,y,evt)

################### REGISTER CONTROLLER CALLBACK HANDLERS TO THE INPUT EVENT STREAMS (TOUCH AND MODEL) #################
mp.irq.registerTouchCallback(TCallback)
mp.irq.enableAll()
m.MCallbackregister(MCallback)


# e.startExtModModifier()
# Start scheduler
loop = mp.uasyncio.get_event_loop()
loop.run_forever()
print("Program finished")