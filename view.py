"""
The view module implements the logic for the view of the MVC Pattern and renders the view tree.
The view module encapsulates the information about the coordinates of the nodes and can return the
widget which has been hit by X- and Y-coordinates.
The view module adds itself to the eventloop and polls the tree for update requests.
"""
from settings import *
if (CPYTHON == True):
    import asyncio as uasyncio
else:
    import uasyncio
import mpywrapper as mp
# print("importing view")

class Node(object):
    """
    A view tree consists of the hierarchical arrangement of the nodes. The root node must be created with
    the class constructor. After creating the root node, according to the structure, the other nodes are added with
     the method add_child, which calls the class constructor internally.
    """
    _evt_loop_initialized = False
    def __init__(self, widget, x_rel, y_rel, width, height, parent = None):
        """
        The init function is called everytime when a new node is created. The first init of the root node is called from
        the controller of MVC pattern directly and only once.
        After having a root node, the nodes are appended according to the structure with the method add_child,
        which calls the init function internally.
        :param widget: The widget which is applied at the node. Can be a container for other widgets too.
        :param x_rel: The X-position of the widget in relation to its parent (from the left border of the parent widget)
        :param y_rel: The Y-position of the widget in relation to its parent (from the upper border of the parent widget)
        :param width: The width of the widget
        :param height: The height of the widget
        :param parent: The parent node
        """
        self._add_to_eventloop()
        self.children = []
        self.parent = parent
        self._updateflag = False
        self._updatepath = False
        self.widget = widget
        if (self.parent != None):
            self.widget.x = self.parent.widget.x + x_rel
            self.widget.y = self.parent.widget.y + y_rel
            self.widget.width = width
            self.widget.height = height
        else:
            self.widget.x = x_rel
            self.widget.y = y_rel
            self.widget.width = width
            self.widget.height = height

    def _add_to_eventloop(self):
        """
        The function adds the view module to the asyncio event loop
        """
        if (Node._evt_loop_initialized == False):
            global uasyncio
            loop = uasyncio.get_event_loop()
            loop.create_task(self.displayTree())
            Node._evt_loop_initialized = True

    def __iter__(self):
        """
        Preorder tree traversal
        :return: returns the next node, beginning from the root node
        """
        if (self != None):
            yield self
            for ch in self.children:
                for n in ch:
                    yield n

    def add_child(self, widget, x_rel, y_rel, width, height):
        """
        Appends a child node to its parent node. Used by the controller of the MVC pattern to assemble the hierarchical
        view tree
        :param self: The (parent-)node to which the child node has to be added, one parent can have more child nodes
        :param widget: The widget which is added as a node to the view tree
        :param x_rel: The X-position of the widget in relation to its parent (from the left border of the parent widget)
        :param y_rel: The Y-position of the widget in relation to its parent (from the upper border of the parent widget)
        :param width: The width of the widget
        :param height: The height of the widget
        :return:
        """
        child = Node(widget, x_rel, y_rel, width, height, self)
        #child.parent = (self)
        self.children.append(child)
        return child

    def append_tree(self, node):
        """
        Append a subtree to a node
        :param self: The node to which the subtree has to be added
        :param node: The highest node in the hierarchy of the subtree which has to be added
        """
        self.children.append(node)
        node.set_update_request()

    def remove_tree(self, node):
        """
        Detach a subtree from a node. The subtree is not deleted, but only detached.
        :param self: The node from which the search of the subtree downwards in the hierarchy will begin
        :param node: The highest node in the hierarchy of the subtree which has to be detached
        """
        if node in self.children:
            mp.gfx.set_color(mp.color_convert("0xFFFFFF"))
            mp.fill_rectangle(node.widget.x, node.widget.y, node.widget.width, node.widget.height)
            self.children.remove(node)

    def set_update_request(self):
        """
        Sets the update request for the node and its subtree. The nodes will be updated by the next run of
        the event loop. Then the view module calls the draw function of the widgets which are implemented
        by the widgets themselves
        :param self: The node which has to be updated
        """
        self._updateflag = True
        for el in self.children:
            for elm in el:
                elm._updatepath = False
                elm._updateflag = False
        while self.parent != None:
            self = self.parent
            self._updatepath = True

    def _preorder_update(self):
        """
        Preorder tree traversal
        """
        if (self != None and self._updatepath == True or self._updateflag == True):
            yield self
            for ch in self.children:
                for n in ch._preorder_update():
                    yield n

    def get_update_node(self):
        """
        Function which is called by the displayTree loop
        :return: yields the next element which has to be updated
        """
        for el in self._preorder_update():
            if (el._updateflag == True):
                yield el

    async def displayTree(self):
        """
        Display the content of the view tree and poll for changed widgets. Called by asyncio event loop
        :return:
        """
        while (True):
            for el in self.get_update_node():
                for e in el:
                    e.widget.draw()

            #reset flags
            for el in self:
                el._updateflag = False
                el._updatepath = False

            if CPYTHON == True:
                await uasyncio.sleep(1/10)
                pass
            else:
                await uasyncio.sleep_ms(5)


    def get_callback_widget(self, x, y):
        """
        Postorder traversal (beginning from the lowest elements in the hierarchy) to determine element which is most
        in the foreground and touched be the x- and y-coordinates
        :param x: absolute (screen) x-coordinate
        :param y: absolute (screen) y-coordinate
        :return: widget which has be touched by the x- and y-coordinate and which ist most in the foreground
        """
        if (self != None):
            if (x > self.widget.x and x < self.widget.x + self.widget.width \
                    and y > self.widget.y and y < self.widget.y + self.widget.height and  self.children == []):
                yield self
            for ch in self.children:
                for n in ch:
                    if (x > n.widget.x and x < n.widget.x + n.widget.width \
                            and y > n.widget.y and y < n.widget.y + n.widget.height and n.children == []):
                        yield n